const speed = 10;
const square = document.querySelector(".square");
const windowWidth = window.innerWidth;
const windowHeight = window.innerHeight;
let col = 0;
let row = 50;
let xDirection = 1;
let yDirection = 1;

const moveSquare = () => {
  if (col + square.clientWidth > windowWidth || col < 0) {
    xDirection = -xDirection;
  }

  if (row + square.clientHeight > windowHeight || row < 0) {
    yDirection = -yDirection;
  }

  col += speed * xDirection;
  row += speed * yDirection;

  square.style.left = col + "px";
  square.style.top = row + "px";
}

setInterval(moveSquare, 1000);
