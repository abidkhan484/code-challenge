# Field Nation Code Challenge
Below are the steps followed to solve the code challenge of Field Nation, including the approach to the solution and the execution of the scripts.

* Clone the repository and run PHP server
```sh
git clone https://gitlab.com/abidkhan484/code-challenge.git
cd code-challenge
php -S localhost:4840
```

## Answer 1
The custom class that extends `ArrayObject` class allows the client script to set key-value pairs. The method displayAsTable is available to show the pairs that have been set based on the given task. The approach to the solution is given below.

1. By defining `__set` and `__get` magic methods, the `CustomArrayObject` class allows objects of the class to set and get properties as if they were array elements. Additionally, the code can set the value of the property "Tony" to "Stark" using object or array syntax and then retrieve it.
2. The client script then instantiates the CustomArrayObject class, sets the key-value pairs using either object or array, and calls the `displayAsTable` method. This method creates a template that displays the key-value pairs that have been set.

The UI can be accessed from the browser by visiting the below URL.
```sh
http://localhost:4840/answer1/index.php
```

## Answer 2
According to the give task, to animate a box within a square div element with a blue background using HTML and CSS, follow these steps:

1. Select the square element using class selector.
2. Get the height and width of the browser window using JavaScript.
3. Use setInterval function to call moveSquare function every 1 second to move the box inside the square element based on its initial position, direction, and pixel value of movement.

The UI can be accessed from the browser by visiting the below URL.
```sh
http://localhost:4840/answer2/animate-box.html
```

## Answer 3
A left join on the `user` table with the `test_result` table is required in this scenario as all user entries is required to appear, even if they have not taken a test.

* Below is the output of the SQL query:

|user_id|first_name|last_name|avg_correct|most_recent_test   |
|:-----:|:--------:|:-------:|:---------:|:-----------------:|
|1      |Bob       |Sanders  |66.00      |2012-09-05 16:22:11|
|2      |Dave      |Greggers |97.00      |2012-09-15 11:27:51|
|3      |Susan     |Bowie    |39.00      |2012-09-15 15:01:49|
|4      |Gary      |Anderson |NULL       |NULL               |
