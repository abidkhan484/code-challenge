
SELECT u.user_id, u.first_name, u.last_name, TRUNCATE(AVG(r.correct), 2) AS avg_correct, MAX(r.time_taken) AS most_recent_test
FROM user AS u
LEFT JOIN test_result AS r ON u.user_id = r.user_id
GROUP BY u.user_id, u.first_name, u.last_name;


/*
+---------+------------+-----------+-------------+---------------------+
| user_id | first_name | last_name | avg_correct | most_recent_test    |
+---------+------------+-----------+-------------+---------------------+
|       1 | Bob        | Sanders   |       66.00 | 2012-09-05 16:22:11 |
|       2 | Dave       | Greggers  |       97.00 | 2012-09-15 11:27:51 |
|       3 | Susan      | Bowie     |       39.00 | 2012-09-15 15:01:49 |
|       4 | Gary       | Anderson  |        NULL | NULL                |
+---------+------------+-----------+-------------+---------------------+
*/

