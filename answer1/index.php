<?php

require dirname(__FILE__)."/CustomArrayObject.php";

$arrayObj = new CustomArrayObject();
$arrayObj['Tony'] = "Parker";
$arrayObj->Tony = "Stark";
$arrayObj->Peter = "Parker";
$arrayObj->displayAsTable();


/*
$myArrayObj = new CustomArrayObject([
  "Tony" => "Stark",
  "Peter" => "Parker",
]);
$myArrayObj->Tony = "Parker";
$myArrayObj->displayAsTable();
*/

?>