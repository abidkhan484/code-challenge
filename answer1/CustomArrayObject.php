<?php


class CustomArrayObject extends ArrayObject
{
  public function __set($name, $val) {
    $this[$name] = $val;
  }

  public function __get($name) {
      return $this[$name];
  }

  public function getAllPairs(): Array {
    return $this->getArrayCopy();
  }

  public function displayAsTable(): void {
    $pairs = $this->getAllPairs();
    include dirname(__FILE__)."/table-template.php";
  }  
}


